package main

import (
	"github.com/urfave/cli"
	"gitlab.com/autom8.network/go-a8-bootstrap/node"
	a8util "gitlab.com/autom8.network/go-a8-util"
	"os"
)

var libp2pPort int64
var bindIP string
var bcastIP string
var idFixer int64

func main() {
	app := cli.NewApp()

	a8util.SetLogLevel(a8util.Trace)

	app.Version = "0.2.0"

	app.Name = "a8node"

	app.Flags = []cli.Flag{
		cli.Int64Flag{
			Name:        "libp2p-port,l",
			Value:       4000,
			Usage:       "port libp2p will open on",
			Destination: &libp2pPort,
		},
		cli.StringFlag{
			Name:        "bindIP,i",
			Value:       "0.0.0.0",
			Usage:       "p2p bind IP adderess (how you will talk to others)",
			Destination: &bindIP,
		},
		cli.StringFlag{
			Name:        "broadcastIP,c",
			Value:       "0.0.0.0",
			Usage:       "p2p broadcast IP adderess (how others will talk to you)",
			Destination: &bcastIP,
		},
		cli.Int64Flag{
			Name:        "idFixer,d",
			Usage:       "locks ipfs id based on input number",
			Value:       0,
			Destination: &idFixer,
		},
	}

	app.Action = func(c *cli.Context) {
		a8util.Log.WithFields(a8util.Fields{
			"libp2pPort": libp2pPort,
			"ipAddr":     bindIP,
			"bcastIP":    bcastIP,
			"idFixer":    idFixer,
		}).Info("Starting Node")

		go node.Node(
			idFixer,
			libp2pPort,
			bindIP,
			bcastIP)

		select {}
	}

	app.Run(os.Args)
}
