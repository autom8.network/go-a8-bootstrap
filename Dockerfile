# Base build image
FROM golang:1.11-alpine AS build-base

# Install some dependencies needed to build the project
RUN apk add bash git gcc g++ libc-dev
ENV D=/go/src/gitlab.com/autom8.network/
WORKDIR $D

# Force the go compiler to use modules
ENV GO111MODULE=on

# We want to populate the module cache based on the go.{mod,sum} files.
COPY go.mod .
COPY go.sum .

RUN echo "cats"

# This is the ‘magic’ step that will download all the dependencies that are specified in 
# the go.mod and go.sum file.
# Because of how the layer caching system works in Docker, the  go mod download 
# command will _ only_ be re-run when the go.mod or go.sum file change 
# (or when we add another docker instruction this line)
RUN go mod download

# This image builds the weavaite server
FROM build-base AS bootstrap-builder


WORKDIR /go/src/gitlab.com/autom8.network/go-a8-bootstrap
# Here we copy the rest of the source code
COPY . .
# And compile the project
RUN CGO_ENABLED=1 GOOS=linux GOARCH=amd64 go build -o a8-bootstrap ./app

#In this last stage, we start from a fresh Alpine image, to reduce the image size and not ship the Go compiler in our production artifacts.
FROM alpine AS a8-bootstrap 

EXPOSE 4002
EXPOSE 3036
EXPOSE 3035

# Finally we copy the statically compiled Go binary.
COPY --from=bootstrap-builder /go/src/gitlab.com/autom8.network/go-a8-bootstrap/a8-bootstrap /bin/a8-bootstrap

CMD ["a8-bootstrap", "--help"]
