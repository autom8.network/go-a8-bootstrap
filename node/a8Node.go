package node

import (
	"context"
	"crypto/rand"
	"fmt"
	"github.com/libp2p/go-libp2p"
	"github.com/libp2p/go-libp2p-crypto"
	libp2pdht "github.com/libp2p/go-libp2p-kad-dht"
	libp2pNet "github.com/libp2p/go-libp2p-net"
	protocol "github.com/libp2p/go-libp2p-protocol"
	"github.com/multiformats/go-multiaddr"
	maddr "github.com/multiformats/go-multiaddr"
	"github.com/sirupsen/logrus"
	"gitlab.com/autom8.network/go-a8-bootstrap/notifier"
	a8util "gitlab.com/autom8.network/go-a8-util"
	"io"
	mrand "math/rand"
)

var a8protocolId = protocol.ID("/a8/0.1.0")

// Node is an a8 node
func Node(
	idFixer int64,
	libp2pPort int64,
	bindIP string,
	broadcastIP string,
) {
	ctx := context.Background()

	// If debug is enabled, use a constant random source to generate the peer ID. Only useful for debugging,
	// off by default. Otherwise, it uses rand.Reader.
	var r io.Reader

	if idFixer != 0 {
		// Use the port number as the randomness source.
		// This will always generate the same host ID on multiple executions, if the same port number is used.
		// Never do this in production code.
		r = mrand.New(mrand.NewSource(int64(idFixer)))
	} else {
		r = rand.Reader
	}

	// Creates a new RSA key pair for this host.
	prvKey, _, err := crypto.GenerateKeyPairWithReader(crypto.RSA, 2048, r)
	if err != nil {
		panic(err)
	}

	multiaddrString := fmt.Sprintf("/ip4/%s/tcp/%d", bindIP, libp2pPort)

	a8util.Log.Info(fmt.Sprintf("The binding multiaddr is %s", multiaddrString))

	// 0.0.0.0 will listen on any interface device.
	sourceMultiAddr, _ := multiaddr.NewMultiaddr(multiaddrString)

	var extMultiAddr maddr.Multiaddr

	multiaddrBroadcast := fmt.Sprintf("/ip4/%s/tcp/%d", broadcastIP, libp2pPort)

	if broadcastIP == "" {
		a8util.Log.Warning("External IP not defined, Peers might not be able to resolve this node if behind NAT\n")
	} else {

		extMultiAddr, err = maddr.NewMultiaddr(multiaddrBroadcast)
		if err != nil {
			a8util.Log.Errorf("Error creating multiaddress: %v\n", err)
			// return nil, err
			return
		}

	}
	addressFactory := func(addrs []maddr.Multiaddr) []maddr.Multiaddr {
		if extMultiAddr != nil {
			addrs = append(addrs, extMultiAddr)
		}
		return addrs
	}

	host, err := libp2p.New(
		ctx,
		libp2p.ListenAddrs(sourceMultiAddr),
		libp2p.Identity(prvKey),
		libp2p.AddrsFactory(addressFactory),
	)

	var id string

	id = host.ID().Pretty()

	//register notify attachment
	host.Network().Notify(notifier.SimpleLogging{})

	multiaddrWithID := fmt.Sprintf("%s/ipfs/%s\n", multiaddrBroadcast, id)

	a8util.Log.Info(fmt.Sprintf("The broadcast multiaddr is %s", multiaddrWithID))

	host.SetStreamHandler(a8protocolId, func(s libp2pNet.Stream) {
		a8util.Log.WithFields(logrus.Fields{
			"protocol": a8protocolId,
		}).Info("Got a new stream!")
	})

	// Start a DHT, for use in peer discovery. We can't just make a new DHT
	// client because we want each peer to maintain its own local copy of the
	// DHT, so that the bootstrapping node of the DHT can go down without
	// inhibiting future peer discovery.
	kademliaDHT, err := libp2pdht.New(ctx, host)
	if err != nil {
		panic(err)
	}

	// Bootstrap the DHT. In the default configuration, this spawns a Background
	// thread that will refresh the peer table every five minutes.
	a8util.Log.Info("Bootstrapping the DHT")
	if err = kademliaDHT.Bootstrap(ctx); err != nil {
		panic(err)
	}

	//this hangs the thread so everything keeps going
	select {}
}

//need to add the ability to look at and trim connections
