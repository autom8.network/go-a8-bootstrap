module gitlab.com/autom8.network/go-a8-bootstrap

go 1.12

require (
	github.com/libp2p/go-libp2p v0.0.2
	github.com/libp2p/go-libp2p-crypto v0.0.1
	github.com/libp2p/go-libp2p-kad-dht v0.0.4
	github.com/libp2p/go-libp2p-net v0.0.1
	github.com/libp2p/go-libp2p-protocol v0.0.1
	github.com/multiformats/go-multiaddr v0.0.1
	github.com/sirupsen/logrus v1.4.2
	github.com/urfave/cli v1.20.0
	gitlab.com/autom8.network/go-a8-util v0.0.0-20190612201753-db513274337b
)
