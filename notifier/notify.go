package notifier

import (
	libp2pNet "github.com/libp2p/go-libp2p-net"
	ma "github.com/multiformats/go-multiaddr"
	a8util "gitlab.com/autom8.network/go-a8-util"
)

//SimpleLogging conforms to Notifee interface, has node log when network actions take place
type SimpleLogging struct {
}

//Listen called when network starts listening on an addr
func (c SimpleLogging) Listen(network libp2pNet.Network, addr ma.Multiaddr) {
	a8util.Log.WithFields(a8util.Fields{
		"multiaddr": addr.String(),
	}).Info("Started Listening")
}

//ListenClose called when network stops listening on an addr
func (c SimpleLogging) ListenClose(network libp2pNet.Network, addr ma.Multiaddr) {
	a8util.Log.WithFields(a8util.Fields{
		"multiaddr": addr.String(),
	}).Info("Stopped listening")
}

//Connected called when a connection opened
func (c SimpleLogging) Connected(network libp2pNet.Network, conn libp2pNet.Conn) {
	a8util.Log.WithFields(a8util.Fields{
		"remotePeer": conn.RemotePeer().Pretty(),
		"localPeer":  conn.LocalPeer().Pretty(),
	}).Info("Opened connection")
}

//Disconnected called when a connection closed
func (c SimpleLogging) Disconnected(network libp2pNet.Network, conn libp2pNet.Conn) {
	a8util.Log.WithFields(a8util.Fields{
		"remotePeer": conn.RemotePeer().Pretty(),
		"localPeer":  conn.LocalPeer().Pretty(),
	}).Info("Closed connection")
}

//OpenedStream called when a stream opened
func (c SimpleLogging) OpenedStream(network libp2pNet.Network, stream libp2pNet.Stream) {
	a8util.Log.WithFields(a8util.Fields{
		"remotePeer": stream.Conn().RemotePeer().Pretty(),
		"localPeer":  stream.Conn().LocalPeer().Pretty(),
	}).Info("Started a Stream")
}

//ClosedStream called when a stream closed
func (c SimpleLogging) ClosedStream(network libp2pNet.Network, stream libp2pNet.Stream) {
	a8util.Log.WithFields(a8util.Fields{
		"remotePeer": stream.Conn().RemotePeer().Pretty(),
		"localPeer":  stream.Conn().LocalPeer().Pretty(),
	}).Info("Closed a Stream")
}
